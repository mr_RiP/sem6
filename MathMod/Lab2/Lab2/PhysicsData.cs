﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    class PhysicsData
    {
        public PhysicsData()
        {
            Rk = 0.2;
            Lk = 60e-6;
            Ck = 150e-6;
            R = 0.35;
            p0 = 0.5;
            Tstart = 300;
            Tw = 2000;
            le = 12;
            Uc0 = 3000;
            I0 = 0.5;
        }

        public PhysicsData
        (
            TextBox txtRk,
            TextBox txtLk,
            TextBox txtCk,
            TextBox txtR,
            TextBox txtp0,
            TextBox txtTstart,
            TextBox txtTw,
            TextBox txtle,
            TextBox txtUc0,
            TextBox txtI0
        )
        {
            Rk = double.Parse(txtRk.Text);
            Lk = double.Parse(txtLk.Text);
            Ck = double.Parse(txtCk.Text);
            R = double.Parse(txtR.Text);
            p0 = double.Parse(txtp0.Text);
            Tstart = double.Parse(txtTstart.Text);
            Tw = double.Parse(txtTw.Text);
            le = double.Parse(txtle.Text);
            Uc0 = double.Parse(txtUc0.Text);
            I0 = double.Parse(txtI0.Text);
        }

        /// <summary>
        /// Сопротивление (0.2 Ом по умолчанию)
        /// </summary>
        public double Rk { get; set; }
        /// <summary>
        /// Индуктивность (60e-6 Гн по умолчанию)
        /// </summary>
        public double Lk { get; set; }
        /// <summary>
        /// Емкость конденсатора (150e-6 Ф по умолчанию)
        /// </summary>
        public double Ck { get; set; }
        /// <summary>
        /// Радиус трубки, фигурирует в верхней границе интеграла (0.35 см по умолчанию)
        /// </summary>
        public double R { get; set; }
        /// <summary>
        /// p0 и Tstart используются в уравнении для нахождения давления p (0.5 атм по умолчанию)
        /// </summary>
        public double p0 { get; set; }
        /// <summary>
        /// p0 и Tstart используются в уравнении для нахождения давления p (300 К по умолчанию)
        /// </summary>
        public double Tstart { get; set; }
        /// <summary>
        /// Возможно, задавались другие значения, скажем,
        /// 3000К или еще что-нибудь, на результат влияет несильно (2000К по умолчанию)
        /// </summary>
        public double Tw { get; set; }
        /// <summary>
        /// Расстояние между электродами лампы (12 см по умолчанию)
        /// </summary>
        public double le { get; set; }
        /// <summary>
        /// Напряжение на конденсаторе в начальный момент времени t = 0 (3000 В по умолчанию)
        /// </summary>
        public double Uc0 { get; set; }
        /// <summary>
        /// Сила тока в цепи в начальный момент времени t = 0 (Допустимые значения 0 .. 2 А, 0.5 А по умолчанию)
        /// </summary>
        public double I0 { get; set; }
    }
}
