﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class TableData
    {
        public TableData()
        {
            InitTable_T0m();
            InitTable_n();
            InitTable_Sigma();
        }

        #region Инизицализация таблиц

        private void InitTable_T0m()
        {
            _table_T0m = new double[9,3]
            {
                { 0.5 , 6400 , 0.40 },
                { 1   , 6790 , 0.55 },
                { 5   , 7150 , 1.70 },
                { 10  , 7270 , 3.0  },
                { 50  , 8010 , 11.0 },
                { 200 , 9185 , 32.0 },
                { 400 , 10010, 40.0 },
                { 800 , 11140, 41.0 },
                { 1200, 12010, 39.0 }
            };
        }

        private void InitTable_n()
        {
            _table_n = new double[19, 4]
            {
                { 1000 , 3.62e+01, 1.086e+02, 1.81e+02 },
                { 1500 , 2.41e+01, 7.242e+01, 1.21e+02 },
                { 2000 , 1.81e+01, 5.431e+01, 9.05e+01 },
                { 2500 , 1.45e+01, 4.345e+01, 7.24e+01 },
                { 3000 , 1.21e+01, 3.621e+01, 6.04e+01 },
                { 3500 , 1.03e+01, 3.104e+01, 5.17e+01 },
                { 4000 , 9.05e+00, 2.716e+01, 4.53e+01 },
                { 4500 , 8.05e+00, 2.414e+01, 4.02e+01 },
                { 5000 , 7.24e+00, 2.173e+01, 3.62e+01 },
                { 6000 , 6.03e+00, 1.810e+01, 3.02e+01 },
                { 7000 , 5.16e+00, 1.550e+01, 2.58e+01 },
                { 8000 , 4.49e+00, 1.350e+01, 2.25e+01 },
                { 9000 , 3.92e+00, 1.188e+01, 1.99e+01 },
                { 10000, 3.39e+00, 1.044e+01, 1.76e+01 },
                { 11000, 2.87e+00, 9.089e+00, 1.54e+01 },
                { 12000, 2.37e+00, 7.784e+00, 1.34e+01 },
                { 13000, 1.94e+00, 6.564e+00, 1.14e+01 },
                { 14000, 1.62e+00, 5.516e+00, 9.72e+00 },
                { 15000, 1.40e+00, 4.695e+00, 8.30e+00 }
            };

            _log_n = InitLogTable(_table_n);
        }

        private void InitTable_Sigma()
        {
            _table_Sigma = new double[19,4]
            {
                { 2000 , 0.525e-3, 0.309e-3, 0.239e-3 },
                { 3000 , 0.525e-2, 0.309e-2, 0.239e-2 },
                { 4000 , 0.525e-1, 0.309e-1, 0.239e-1 },
                { 5000 , 0.442e+0, 0.270e+0, 0.214e+0 },
                { 6000 , 0.283e1 , 0.205e1 , 0.175e1  },
                { 7000 , 0.741e1 , 0.606e1 , 0.545e1  },
                { 8000 , 0.138e2 , 0.120e2 , 0.111e2  },
                { 9000 , 0.220e2 , 0.199e2 , 0.188e2  },
                { 10000, 0.317e2 , 0.296e2 , 0.284e2  },
                { 11000, 0.428e2 , 0.411e2 , 0.399e2  },
                { 12000, 0.547e2 , 0.541e2 , 0.533e2  },
                { 13000, 0.664e2 , 0.677e2 , 0.677e2  },
                { 14000, 0.769e2 , 0.815e2 , 0.825e2  },
                { 15000, 0.861e2 , 0.938e2 , 0.965e2  },
                { 16000, 0.941e2 , 0.105e3 , 0.109e3  },
                { 17000, 0.102e3 , 0.115e3 , 0.120e3  },
                { 18000, 0.112e3 , 0.124e3 , 0.131e3  },
                { 19000, 0.126e3 , 0.135e3 , 0.142e3  },
                { 20000, 0.149e3 , 0.150e3 , 0.154e3  }
            };

            _log_Sigma = InitLogTable(_table_Sigma);
        }

        private double[,] InitLogTable(double[,] table)
        {
            int rows = table.GetLength(0);
            int cols = table.GetLength(1);
            var logTable = new double[rows, cols - 1];

            for (int i = 0; i < rows; ++i)
                for (int j = 1; j < cols; ++j)
                    logTable[i, j - 1] = Math.Log(table[i, j]);

            return logTable;
        }

        #endregion

        public double T0(double I)
        {
            return LinearInterpol(_table_T0m, 1, I);
        }

        public double m(double I)
        {
            return LinearInterpol(_table_T0m, 2, I);
        }

        public double n(double T, double p)
        {
            return SquareInterpol(_table_n, _log_n, T, p);
        }


        public double Sigma(double T, double p)
        {
            return SquareInterpol(_table_Sigma, _log_Sigma, T, p);
        }


        private double[,] _table_T0m;
        private double[,] _table_n;
        private double[,] _log_n;
        private double[,] _table_Sigma;
        private double[,] _log_Sigma;

        static private double p(int index)
        {
            switch (index)
            {
                case 1: return 5;
                case 2: return 15;
                case 3: return 25;
                default: throw new IndexOutOfRangeException();
            }
        }

        static private int FirstColIndex(double value)
        {
            return value < 15 ? 1 : 2;
        }

        static private int FirstRowIndex(double[,] table, double value)
        {
            int index = table.GetLength(0) - 1;
            if (value > table[index, 0])
                return index - 1;

            index = 0;
            while (value > table[index, 0])
                ++index;

            return index;
        }

        static private double LinearInterpol(double[,] table, int colIndex, double value)
        {
            int i0 = FirstRowIndex(table, value);
            int i1 = i0 + 1;

            return table[i0, colIndex] + (table[i1, colIndex] - table[i0, colIndex]) /
                (table[i1, 0] - table[i0, 0]) * (value - table[i0, 0]);
        }

        static private double SquareInterpol(double[,] table, double[,] log, double rowValue, double colValue)
        {
            int rowIndex = FirstRowIndex(table, rowValue);
            int colIndex = FirstColIndex(colValue);

            double log0 = log[rowIndex, colIndex - 1] + 
                (log[rowIndex + 1, colIndex - 1] - log[rowIndex, colIndex - 1]) /
                (table[rowIndex + 1, 0] - table[rowIndex, 0]) *
                (rowValue - table[rowIndex, 0]);

            double log1 = log[rowIndex, colIndex] +
                (log[rowIndex + 1, colIndex] - log[rowIndex, colIndex]) /
                (table[rowIndex + 1, 0] - table[rowIndex, 0]) *
                (rowValue - table[rowIndex, 0]);

            return Math.Exp(log0) + (log1 - log0) / (p(colIndex + 1) - p(colIndex)) * (colValue - p(colIndex));
        }
    }
}
