﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class ModelCalculator
    {
        public ModelCalculator()
        {
            PhysicsData = new PhysicsData();
            TableData = new TableData();
        }

        public ModelCalculator(PhysicsData physData)
        {
            PhysicsData = physData;
            TableData = new TableData();
        }

        public void Calculate(double step, int count)
        {
            NullPreviousResults();

            double Rp = Calculate_Rp();
        }


        private void NullPreviousResults()
        {
            t = null;
            Uc = null;
            Ucp = null;
            Rp = null;
        }

        private double Calculate_Rp(double I, int n = 41)
        {
            double h = PhysicsData.R / (n - 1);
            double[] T = Calculate_T(I, h, n);
            double p = Calculate_p(T, h, n);


        }

        private double Calculate_p(double[] T, double h, int n, double eps = 1e-5)
        {
            double a = 2.0 / (PhysicsData.R * PhysicsData.R);
            double b = (PhysicsData.p0 * 7242.0) / PhysicsData.Tstart;

            double l = 0;
            double r = PhysicsData.R;

            while (Math.Abs(r - l) > eps)
            {
                double m = (r - l) / 2.0;
                if (a*Integral())
            }

            // Доделать!
        }

        private double Integral(double p, double[] T, double h, int n)
        {
            n = n - 1;
            double sum = TableData.n(T[n], p) * (n * h);
            for (int i = 1; i < n; ++i)
            {
                double func = TableData.n(T[i], p) * (i * h);
                if (i % 2 == 0)
                    sum += 2.0 * func;
                else
                    sum += 4.0 * func;
            }
            return sum * h / 3.0;
        }

        private double[] Calculate_T(double I, double h, int n)
        {
            double[] T = new double[n];
            for (int i = 0; i < n; ++i)
            {
                double T0 = TableData.T0(I);
                double m = TableData.m(I);
                double r = i * h;
                T[i] = (PhysicsData.Tw - T0) * Math.Pow((r / PhysicsData.R), m) + T0;
            }
            return T;
        }


        private TableData TableData { get; set; }
        public PhysicsData PhysicsData { get; set; }

        public double[] t { get; private set; }
        public double[] I { get; private set; }
        public double[] Uc { get; private set; }
        public double[] Ucp { get; private set; }
        public double[] Rp { get; private set; }
    }
}
