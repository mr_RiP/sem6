﻿namespace Lab2
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chrtUpperGraph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chrtLowerGraph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.startButton = new System.Windows.Forms.Button();
            this.physicsDataBox = new System.Windows.Forms.GroupBox();
            this.lblI0 = new System.Windows.Forms.Label();
            this.lblUc0 = new System.Windows.Forms.Label();
            this.lblle = new System.Windows.Forms.Label();
            this.lblTw = new System.Windows.Forms.Label();
            this.lblTstart = new System.Windows.Forms.Label();
            this.lblp0 = new System.Windows.Forms.Label();
            this.lblR = new System.Windows.Forms.Label();
            this.lblCk = new System.Windows.Forms.Label();
            this.lblLk = new System.Windows.Forms.Label();
            this.lblRk = new System.Windows.Forms.Label();
            this.txtle = new System.Windows.Forms.TextBox();
            this.txtUc0 = new System.Windows.Forms.TextBox();
            this.txtI0 = new System.Windows.Forms.TextBox();
            this.txtCk = new System.Windows.Forms.TextBox();
            this.txtR = new System.Windows.Forms.TextBox();
            this.txtp0 = new System.Windows.Forms.TextBox();
            this.txtTstart = new System.Windows.Forms.TextBox();
            this.txtTw = new System.Windows.Forms.TextBox();
            this.txtLk = new System.Windows.Forms.TextBox();
            this.txtRk = new System.Windows.Forms.TextBox();
            this.calcInitDataBox = new System.Windows.Forms.GroupBox();
            this.lblStep = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.txtStep = new System.Windows.Forms.TextBox();
            this.txtCount = new System.Windows.Forms.TextBox();
            this.methodBox = new System.Windows.Forms.GroupBox();
            this.rbImplicitMethod = new System.Windows.Forms.RadioButton();
            this.rbRungeKuttaMethod = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.chrtUpperGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrtLowerGraph)).BeginInit();
            this.physicsDataBox.SuspendLayout();
            this.calcInitDataBox.SuspendLayout();
            this.methodBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // chrtUpperGraph
            // 
            chartArea1.Name = "ChartArea1";
            this.chrtUpperGraph.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chrtUpperGraph.Legends.Add(legend1);
            this.chrtUpperGraph.Location = new System.Drawing.Point(183, 12);
            this.chrtUpperGraph.Name = "chrtUpperGraph";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chrtUpperGraph.Series.Add(series1);
            this.chrtUpperGraph.Size = new System.Drawing.Size(422, 230);
            this.chrtUpperGraph.TabIndex = 0;
            this.chrtUpperGraph.Text = "chart1";
            // 
            // chrtLowerGraph
            // 
            chartArea2.Name = "ChartArea1";
            this.chrtLowerGraph.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chrtLowerGraph.Legends.Add(legend2);
            this.chrtLowerGraph.Location = new System.Drawing.Point(183, 251);
            this.chrtLowerGraph.Name = "chrtLowerGraph";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chrtLowerGraph.Series.Add(series2);
            this.chrtLowerGraph.Size = new System.Drawing.Size(422, 230);
            this.chrtLowerGraph.TabIndex = 1;
            this.chrtLowerGraph.Text = "chart2";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(12, 458);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(165, 23);
            this.startButton.TabIndex = 2;
            this.startButton.Text = "Запуск";
            this.startButton.UseVisualStyleBackColor = true;
            // 
            // physicsDataBox
            // 
            this.physicsDataBox.Controls.Add(this.lblI0);
            this.physicsDataBox.Controls.Add(this.lblUc0);
            this.physicsDataBox.Controls.Add(this.lblle);
            this.physicsDataBox.Controls.Add(this.lblTw);
            this.physicsDataBox.Controls.Add(this.lblTstart);
            this.physicsDataBox.Controls.Add(this.lblp0);
            this.physicsDataBox.Controls.Add(this.lblR);
            this.physicsDataBox.Controls.Add(this.lblCk);
            this.physicsDataBox.Controls.Add(this.lblLk);
            this.physicsDataBox.Controls.Add(this.lblRk);
            this.physicsDataBox.Controls.Add(this.txtle);
            this.physicsDataBox.Controls.Add(this.txtUc0);
            this.physicsDataBox.Controls.Add(this.txtI0);
            this.physicsDataBox.Controls.Add(this.txtCk);
            this.physicsDataBox.Controls.Add(this.txtR);
            this.physicsDataBox.Controls.Add(this.txtp0);
            this.physicsDataBox.Controls.Add(this.txtTstart);
            this.physicsDataBox.Controls.Add(this.txtTw);
            this.physicsDataBox.Controls.Add(this.txtLk);
            this.physicsDataBox.Controls.Add(this.txtRk);
            this.physicsDataBox.Location = new System.Drawing.Point(12, 12);
            this.physicsDataBox.Name = "physicsDataBox";
            this.physicsDataBox.Size = new System.Drawing.Size(165, 284);
            this.physicsDataBox.TabIndex = 3;
            this.physicsDataBox.TabStop = false;
            this.physicsDataBox.Text = "Физические параметры";
            // 
            // lblI0
            // 
            this.lblI0.AutoSize = true;
            this.lblI0.Location = new System.Drawing.Point(18, 256);
            this.lblI0.Name = "lblI0";
            this.lblI0.Size = new System.Drawing.Size(16, 13);
            this.lblI0.TabIndex = 18;
            this.lblI0.Text = "I0";
            // 
            // lblUc0
            // 
            this.lblUc0.AutoSize = true;
            this.lblUc0.Location = new System.Drawing.Point(11, 230);
            this.lblUc0.Name = "lblUc0";
            this.lblUc0.Size = new System.Drawing.Size(27, 13);
            this.lblUc0.TabIndex = 17;
            this.lblUc0.Text = "Uc0";
            // 
            // lblle
            // 
            this.lblle.AutoSize = true;
            this.lblle.Location = new System.Drawing.Point(19, 204);
            this.lblle.Name = "lblle";
            this.lblle.Size = new System.Drawing.Size(15, 13);
            this.lblle.TabIndex = 16;
            this.lblle.Text = "le";
            // 
            // lblTw
            // 
            this.lblTw.AutoSize = true;
            this.lblTw.Location = new System.Drawing.Point(16, 178);
            this.lblTw.Name = "lblTw";
            this.lblTw.Size = new System.Drawing.Size(22, 13);
            this.lblTw.TabIndex = 15;
            this.lblTw.Text = "Tw";
            // 
            // lblTstart
            // 
            this.lblTstart.AutoSize = true;
            this.lblTstart.Location = new System.Drawing.Point(11, 152);
            this.lblTstart.Name = "lblTstart";
            this.lblTstart.Size = new System.Drawing.Size(31, 13);
            this.lblTstart.TabIndex = 14;
            this.lblTstart.Text = "Tнач";
            // 
            // lblp0
            // 
            this.lblp0.AutoSize = true;
            this.lblp0.Location = new System.Drawing.Point(15, 126);
            this.lblp0.Name = "lblp0";
            this.lblp0.Size = new System.Drawing.Size(19, 13);
            this.lblp0.TabIndex = 13;
            this.lblp0.Text = "p0";
            // 
            // lblR
            // 
            this.lblR.AutoSize = true;
            this.lblR.Location = new System.Drawing.Point(16, 100);
            this.lblR.Name = "lblR";
            this.lblR.Size = new System.Drawing.Size(15, 13);
            this.lblR.TabIndex = 12;
            this.lblR.Text = "R";
            // 
            // lblCk
            // 
            this.lblCk.AutoSize = true;
            this.lblCk.Location = new System.Drawing.Point(15, 74);
            this.lblCk.Name = "lblCk";
            this.lblCk.Size = new System.Drawing.Size(20, 13);
            this.lblCk.TabIndex = 11;
            this.lblCk.Text = "Ck";
            // 
            // lblLk
            // 
            this.lblLk.AutoSize = true;
            this.lblLk.Location = new System.Drawing.Point(16, 48);
            this.lblLk.Name = "lblLk";
            this.lblLk.Size = new System.Drawing.Size(19, 13);
            this.lblLk.TabIndex = 10;
            this.lblLk.Text = "Lk";
            // 
            // lblRk
            // 
            this.lblRk.AutoSize = true;
            this.lblRk.Location = new System.Drawing.Point(16, 22);
            this.lblRk.Name = "lblRk";
            this.lblRk.Size = new System.Drawing.Size(21, 13);
            this.lblRk.TabIndex = 6;
            this.lblRk.Text = "Rk";
            // 
            // txtle
            // 
            this.txtle.Location = new System.Drawing.Point(55, 201);
            this.txtle.Name = "txtle";
            this.txtle.Size = new System.Drawing.Size(100, 20);
            this.txtle.TabIndex = 9;
            this.txtle.Text = "12";
            // 
            // txtUc0
            // 
            this.txtUc0.Location = new System.Drawing.Point(55, 227);
            this.txtUc0.Name = "txtUc0";
            this.txtUc0.Size = new System.Drawing.Size(100, 20);
            this.txtUc0.TabIndex = 8;
            this.txtUc0.Text = "3000";
            // 
            // txtI0
            // 
            this.txtI0.Location = new System.Drawing.Point(55, 253);
            this.txtI0.Name = "txtI0";
            this.txtI0.Size = new System.Drawing.Size(100, 20);
            this.txtI0.TabIndex = 7;
            this.txtI0.Text = "0,5";
            // 
            // txtCk
            // 
            this.txtCk.Location = new System.Drawing.Point(54, 71);
            this.txtCk.Name = "txtCk";
            this.txtCk.Size = new System.Drawing.Size(100, 20);
            this.txtCk.TabIndex = 6;
            this.txtCk.Text = "150e-6";
            // 
            // txtR
            // 
            this.txtR.Location = new System.Drawing.Point(54, 97);
            this.txtR.Name = "txtR";
            this.txtR.Size = new System.Drawing.Size(100, 20);
            this.txtR.TabIndex = 5;
            this.txtR.Text = "0,35";
            // 
            // txtp0
            // 
            this.txtp0.Location = new System.Drawing.Point(54, 123);
            this.txtp0.Name = "txtp0";
            this.txtp0.Size = new System.Drawing.Size(100, 20);
            this.txtp0.TabIndex = 4;
            this.txtp0.Text = "0,5";
            // 
            // txtTstart
            // 
            this.txtTstart.Location = new System.Drawing.Point(54, 149);
            this.txtTstart.Name = "txtTstart";
            this.txtTstart.Size = new System.Drawing.Size(100, 20);
            this.txtTstart.TabIndex = 3;
            this.txtTstart.Text = "300";
            // 
            // txtTw
            // 
            this.txtTw.Location = new System.Drawing.Point(55, 175);
            this.txtTw.Name = "txtTw";
            this.txtTw.Size = new System.Drawing.Size(100, 20);
            this.txtTw.TabIndex = 2;
            this.txtTw.Text = "2000";
            // 
            // txtLk
            // 
            this.txtLk.Location = new System.Drawing.Point(54, 45);
            this.txtLk.Name = "txtLk";
            this.txtLk.Size = new System.Drawing.Size(100, 20);
            this.txtLk.TabIndex = 1;
            this.txtLk.Text = "60e-6";
            // 
            // txtRk
            // 
            this.txtRk.Location = new System.Drawing.Point(54, 19);
            this.txtRk.Name = "txtRk";
            this.txtRk.Size = new System.Drawing.Size(100, 20);
            this.txtRk.TabIndex = 0;
            this.txtRk.Text = "0,2";
            // 
            // calcInitDataBox
            // 
            this.calcInitDataBox.Controls.Add(this.lblStep);
            this.calcInitDataBox.Controls.Add(this.lblCount);
            this.calcInitDataBox.Controls.Add(this.txtStep);
            this.calcInitDataBox.Controls.Add(this.txtCount);
            this.calcInitDataBox.Location = new System.Drawing.Point(12, 302);
            this.calcInitDataBox.Name = "calcInitDataBox";
            this.calcInitDataBox.Size = new System.Drawing.Size(165, 75);
            this.calcInitDataBox.TabIndex = 4;
            this.calcInitDataBox.TabStop = false;
            this.calcInitDataBox.Text = "Параметры вычисления";
            // 
            // lblStep
            // 
            this.lblStep.AutoSize = true;
            this.lblStep.Location = new System.Drawing.Point(8, 50);
            this.lblStep.Name = "lblStep";
            this.lblStep.Size = new System.Drawing.Size(27, 13);
            this.lblStep.TabIndex = 3;
            this.lblStep.Text = "Шаг";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(6, 23);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(98, 13);
            this.lblCount.TabIndex = 2;
            this.lblCount.Text = "Количество узлов";
            // 
            // txtStep
            // 
            this.txtStep.Location = new System.Drawing.Point(110, 47);
            this.txtStep.Name = "txtStep";
            this.txtStep.Size = new System.Drawing.Size(45, 20);
            this.txtStep.TabIndex = 1;
            this.txtStep.Text = "1e-5";
            // 
            // txtCount
            // 
            this.txtCount.Location = new System.Drawing.Point(110, 20);
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(45, 20);
            this.txtCount.TabIndex = 0;
            this.txtCount.Text = "70";
            // 
            // methodBox
            // 
            this.methodBox.Controls.Add(this.rbImplicitMethod);
            this.methodBox.Controls.Add(this.rbRungeKuttaMethod);
            this.methodBox.Location = new System.Drawing.Point(12, 383);
            this.methodBox.Name = "methodBox";
            this.methodBox.Size = new System.Drawing.Size(165, 69);
            this.methodBox.TabIndex = 5;
            this.methodBox.TabStop = false;
            this.methodBox.Text = "Метод вычисления";
            // 
            // rbImplicitMethod
            // 
            this.rbImplicitMethod.AutoSize = true;
            this.rbImplicitMethod.Location = new System.Drawing.Point(22, 42);
            this.rbImplicitMethod.Name = "rbImplicitMethod";
            this.rbImplicitMethod.Size = new System.Drawing.Size(71, 17);
            this.rbImplicitMethod.TabIndex = 1;
            this.rbImplicitMethod.Text = "Неявный";
            this.rbImplicitMethod.UseVisualStyleBackColor = true;
            // 
            // rbRungeKuttaMethod
            // 
            this.rbRungeKuttaMethod.AutoSize = true;
            this.rbRungeKuttaMethod.Checked = true;
            this.rbRungeKuttaMethod.Location = new System.Drawing.Point(22, 19);
            this.rbRungeKuttaMethod.Name = "rbRungeKuttaMethod";
            this.rbRungeKuttaMethod.Size = new System.Drawing.Size(85, 17);
            this.rbRungeKuttaMethod.TabIndex = 0;
            this.rbRungeKuttaMethod.TabStop = true;
            this.rbRungeKuttaMethod.Text = "Рунге-Кутта";
            this.rbRungeKuttaMethod.UseVisualStyleBackColor = true;
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 487);
            this.Controls.Add(this.methodBox);
            this.Controls.Add(this.calcInitDataBox);
            this.Controls.Add(this.physicsDataBox);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.chrtLowerGraph);
            this.Controls.Add(this.chrtUpperGraph);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(628, 526);
            this.Name = "frmMainForm";
            this.Text = "Лабораторная работа 2";
            ((System.ComponentModel.ISupportInitialize)(this.chrtUpperGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrtLowerGraph)).EndInit();
            this.physicsDataBox.ResumeLayout(false);
            this.physicsDataBox.PerformLayout();
            this.calcInitDataBox.ResumeLayout(false);
            this.calcInitDataBox.PerformLayout();
            this.methodBox.ResumeLayout(false);
            this.methodBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chrtUpperGraph;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrtLowerGraph;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.GroupBox physicsDataBox;
        private System.Windows.Forms.GroupBox calcInitDataBox;
        private System.Windows.Forms.GroupBox methodBox;
        private System.Windows.Forms.Label lblI0;
        private System.Windows.Forms.Label lblUc0;
        private System.Windows.Forms.Label lblle;
        private System.Windows.Forms.Label lblTw;
        private System.Windows.Forms.Label lblTstart;
        private System.Windows.Forms.Label lblp0;
        private System.Windows.Forms.Label lblR;
        private System.Windows.Forms.Label lblCk;
        private System.Windows.Forms.Label lblLk;
        private System.Windows.Forms.Label lblRk;
        private System.Windows.Forms.TextBox txtle;
        private System.Windows.Forms.TextBox txtUc0;
        private System.Windows.Forms.TextBox txtI0;
        private System.Windows.Forms.TextBox txtCk;
        private System.Windows.Forms.TextBox txtR;
        private System.Windows.Forms.TextBox txtp0;
        private System.Windows.Forms.TextBox txtTstart;
        private System.Windows.Forms.TextBox txtTw;
        private System.Windows.Forms.TextBox txtLk;
        private System.Windows.Forms.TextBox txtRk;
        private System.Windows.Forms.TextBox txtStep;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.Label lblStep;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.RadioButton rbImplicitMethod;
        private System.Windows.Forms.RadioButton rbRungeKuttaMethod;
    }
}

