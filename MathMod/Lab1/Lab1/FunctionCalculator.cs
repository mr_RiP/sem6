﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class FunctionCalculator
    {
        public void Run()
        {
            Initialize();
            Calculate();
            PrintTable();
        }

        private void Initialize()
        {
            Console.Clear();
            _step = AskUser("Введите шаг:");
            _x_max = AskUser("Введите максимально возможное значение X:");
            _eps = 0.00000001;
            Console.WriteLine();
        }

        private void Calculate()
        {
            _x = SetX();

            var methods = SetMethods();

            _y = new double[methods.Length][];
            for (int i = 0; i < methods.Length; ++i)
                _y[i] = methods[i].Calculate(_x);   
        }

        private void PrintTable()
        {
            PrintTableHead();
            PrintTableLines();
        }

        private void PrintTableHead()
        {
            Console.WriteLine("===============================================================================");
            Console.WriteLine("    X    |                 Пикар                 | Рунге-  | Явный   | Неявный ");
            Console.WriteLine("         |    1    |    2    |    3    |    4    |   Кутта |   метод |    метод");
            Console.WriteLine("===============================================================================");
        }

        private void PrintTableLines()
        {
            for (int i = 0; i < _x.Length; ++i)
            {
                Console.Write("{0,9:##0.00000}", _x[i]);
                for (int j = 0; j < _y.Length; ++j)
                    Console.Write("|{0,9:##0.00000}", _y[j][i]);
                Console.WriteLine();
            }
        }

        private double[] SetX()
        {
            var x_list = new List<double>();
            for (double x = 0.0; x <= (_x_max + _eps) ; x += _step)
                x_list.Add(x);
            return x_list.ToArray();
        }
        
        private ICalculationMethod[] SetMethods()
        {
            var methods = new List<ICalculationMethod>();
            for (int i = 1; i <= 4; ++i)
                methods.Add(new Picard(i));

            methods.Add(new RungeKutta2(_step));
            methods.Add(new ExplicitMethod(_step));
            methods.Add(new ImplicitMethod(_step));

            return methods.ToArray();
        }

        private double AskUser(string msg)
        {
            Console.WriteLine(msg);
            Console.Write("-> ");
            return Double.Parse(Console.ReadLine());
        }

        private double _step;
        private double _x_max;
        private double _eps;

        private double[] _x;
        private double[][] _y;
    }
}
