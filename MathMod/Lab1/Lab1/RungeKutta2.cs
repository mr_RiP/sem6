﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class RungeKutta2 : ICalculationMethod
    {
        public RungeKutta2(double h)
        {
            Step = h;
        }

        public double Step { get; set; }

        public static double CalculateSingle(double prev_x, double prev_y, double h)
        {
            return prev_y + h * Function(prev_x + h / 2.0, prev_y + h / 2.0 * Function(prev_x, prev_y));
        }

        private static double Function(double x, double u)
        {
            return x * x + u * u;
        }

        public double[] Calculate(double[] x_values)
        {
            var y_values = new List<double> { 0.0 };
            for (int i = 1; i < x_values.Length; ++i)
                y_values.Add(CalculateSingle(x_values[i - 1], y_values[i - 1], Step));

            return y_values.ToArray();
        }
    }
}
