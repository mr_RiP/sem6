﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class ImplicitMethod : ICalculationMethod
    {
        public ImplicitMethod(double h)
        {
            Step = h;
        }

        public double Step { get; set; }

        public static double CalculateSingle(double x, double prev_y, double h)
        {
            double d = Math.Sqrt(1.0 - 4.0 * h * (prev_y + h * x * x));
            double root = (1.0 - d) / (2.0 * h);
            if (double.IsNaN(root))
                root = (1.0 + d) / (2.0 * h);
            return root;
        }

        public double[] Calculate(double[] x_values)
        {
            var y_values = new List<double> { 0.0 };
            for (int i = 1; i < x_values.Length; ++i)
                y_values.Add(CalculateSingle(x_values[i], y_values[i - 1], Step));

            return y_values.ToArray();
        }
    }
}
