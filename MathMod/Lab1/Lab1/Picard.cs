﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{

    class Picard : ICalculationMethod
    {
        public Picard(int precision)
        {
            Precision = precision;
        }

        public int Precision { get; set; }

        public static double CalculateSingle(int precision, double x)
        {
      
            switch (precision)
            {
                case 0:
                    return 0;
                case 1:
                    return Math.Pow(x, 3.0) / 3.0;
                case 2:
                    return Math.Pow(x, 3.0) / 3.0 * (1.0 + Math.Pow(x, 4.0) / 21.0);
                case 3:
                    return Math.Pow(x, 3.0) / 3.0 * (1.0 + Math.Pow(x, 4.0) / 21.0 +
                        2.0 * Math.Pow(x, 8.0) / 693.0 + Math.Pow(x, 12.0) / 19845.0);
                case 4:
                    return Math.Pow(x, 3.0) / 3.0 * (1.0 + x / 4.0 + Math.Pow(x, 5.0) / (21.0 * 8.0) +
                        2 * Math.Pow(x, 10.0) / (693.0 + 13.0) + Math.Pow(x, 13.0) / (19845.0 * 16.0));
                    /* 
                    return (x * x * x) / 3.0 + Math.Pow(x, 7.0) / 63.0 + (2.0 * Math.Pow(x, 11.0)) / 2079.0 +
                        (13.0 * Math.Pow(x, 15.0)) / 218295.0 + (82.0 * Math.Pow(x, 19.0)) / 37328445.0 + 
                        (662.0 * Math.Pow(x, 23.0)) / 10438212015.0 + (4.0 * Math.Pow(x, 27.0)) / 3341878155.0 +
                        Math.Pow(x, 31.0) / 109876902975.0;
                    */
                default:
                    throw new NotImplementedException();
            }
        }

        public double[] Calculate(double[] x_values)
        {
            var y_values = new List<double>();
            for (int i = 0; i < x_values.Length; ++i)
                y_values.Add(CalculateSingle(Precision, x_values[i]));

            return y_values.ToArray();
        }
    }
}
