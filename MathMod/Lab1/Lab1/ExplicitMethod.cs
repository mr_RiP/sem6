﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class ExplicitMethod : ICalculationMethod
    {
        public ExplicitMethod(double h)
        {
            Step = h;
        }

        public double Step { get; set; }

        public static double CalculateSingle(double prev_x, double prev_y, double h)
        {
            return prev_y + h * (prev_x * prev_x + prev_y * prev_y);
        }

        public double[] Calculate(double[] x_values)
        {
            var y_values = new List<double> { 0.0 };
            for (int i = 1; i < x_values.Length; ++i)
                y_values.Add(CalculateSingle(x_values[i - 1], y_values[i - 1], Step));

            return y_values.ToArray();
        }
    }
}
