#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <syslog.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <signal.h>

#define LOCKFILE "/var/run/mydaemon.pid"
#define LOCKMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)
#define BUFSIZE 16

void daemonize(const char* cmd)
{
	int i, fd0, fd1, fd2;
	pid_t pid;
	struct rlimit rl;
	struct sigaction sa;
	
	// сбрасываем маску режима создания файла
	umask(0);
	
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
		exit(1);
	
	// утрачиваем управляющий терминал
	if ((pid = fork()) < 0)
		exit(1);
	else if (pid != 0)
		exit(0);
		
		
	// маскируем сигнал об утрате терминала
	// и становимся лидером сессии	
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGHUP, &sa, NULL) < 0)
		exit(1);
	if ((pid = fork()) < 0)
		exit(1);
	else if (pid != 0)
		exit(0);
	
	setsid();
	
	// устанавливаем рабочим корневой каталог
	if (chdir("/") < 0)
		exit(1);	
	
	// закрыть все открытые файловые дескрипторы
	if (rl.rlim_max == RLIM_INFINITY)
		rl.rlim_max = 1024;
	for (i = 0; i < rl.rlim_max; ++i)
		close(i);
	
	// Присоединить файловые дескрипторы 0, 1 и 2 к /dev/null
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);
	
	// инициализировать файл журнала
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if (fd0 != 0 || fd1 != 1 || fd2 != 2)
	{
		syslog(LOG_ERR, "wrong file descriptors %d %d %d",
				fd0, fd1, fd2);
		exit(1);
	}
}

int already_running(void)
{
	int fd;
	char buf[BUFSIZE];
	
	fd = open (LOCKFILE, O_RDWR|O_CREAT, LOCKMODE);
	if (fd < 0)
	{
		syslog (LOG_ERR, "unable to open %s: %s", LOCKFILE, strerror(errno));
		exit(1);
	}
	
	if (flock (fd, LOCK_EX|LOCK_NB) != 0)
	{
		syslog(LOG_ERR, "unable to lock %s: %s", LOCKFILE, strerror(errno));
		close(fd);
		return 1;
	}
	
	snprintf (buf, BUFSIZE, "%d", getpid());
	write (fd, buf, strlen(buf));
	return 0;
}

void daemon_body()
{
	int count = 1;
	while (1)
	{
		syslog(LOG_INFO, "record #%d", count++);
		sleep(10);
	}
}

int main (void)
{
	daemonize("mydaemon");
	if (already_running())
		exit(0);
	daemon_body();
	return 0;
}