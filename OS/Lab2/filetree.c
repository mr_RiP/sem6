#include "apue.h"
#include <dirent.h>
#include <limits.h>
#include <unistd.h>

typedef int Myfunc(const char *, const struct stat *, int);
static Myfunc myfunc;
static int myftw(char *, Myfunc *);
static int dopath(Myfunc *, unsigned int);
static long nreg, ndir, nblk, nchr, nfifo, nslink, nsock, ntot;

int main(int argc, char *argv[])
{
	int ret;
	
	if (argc != 2)
		err_quit("Использование: ftw <начальный_каталог>");
	
	ret = myftw(argv[1], myfunc); /* выполняет всю работу */

	ntot = nreg + ndir + nblk + nchr + nfifo + nslink + nsock;
	if (ntot == 0)
		ntot = 1;/* во избежание деления на 0; вывести 0 для всех счетчиков */
		
	printf("\n");
	printf("обычные файлы = %5.2f%% (%ld)\n", nreg*100.0/ntot,
	nreg);
	printf("каталоги = %5.2f%% (%ld)\n", ndir*100.0/ntot,
	ndir);
	printf("специальные файлы блочных устройств = %5.2f%% (%ld)\n", nblk*100.0/ntot, nblk);
	printf("специальные файлы символьных устройств = %5.2f%% (%ld)\n", nchr*100.0/ntot,
	nchr);
	printf("FIFO = %5.2f%% (%ld)\n", nfifo*100.0/ntot,
	nfifo);
	printf("символические ссылки = %5.2f%% (%ld)\n", nslink*100.0/ntot,
	nslink);
	printf("сокеты = %5.2f%% (%ld)\n", nsock*100.0/ntot,
	nsock);

	exit(ret);
}

static char *get_rel(char *path)
{
	if (strcmp(path, "/") == 0)
		return path;
	
	char *ret = strrchr(path, '/');
	
	if (ret)
		return ret + 1;
	else
		return path;
}

#define FTW_F 1 	/* файл, не являющийся каталогом */
#define FTW_D 2 	/* каталог */
#define FTW_DNR 3 	/* каталог, который не доступен для чтения */
#define FTW_NS 4 	/* файл, информацию о котором невозможно получить с помощью stat */
static char *fullpath; /* полный путь к каждому из файлов */
static int myftw(char *pathname, Myfunc *func) /* возвращаем то, что вернула функция func() */
{
	int len;
		
	fullpath = path_alloc(&len);

	chdir(pathname);
	chdir("..");
	
	strcpy(fullpath, get_rel(pathname));

	int ret = dopath(func, 0);

	free(fullpath);
	
	return ret;
}

static int dopath(Myfunc* func, unsigned int level)
{
	struct stat	statbuf;
	struct dirent *dirp;
	DIR *dp;
	int	ret;
//	char *ptr;
	unsigned int i;
	
	ret = 0;
	//err_msg("%s\n", fullpath);
	
	if (lstat(fullpath, &statbuf) < 0) 			/* ошибка вызова функции stat */
		return func(fullpath, &statbuf, FTW_NS);

	if (S_ISDIR(statbuf.st_mode) == 0) 			/* не каталог */
		return func(fullpath, &statbuf, FTW_F);

	if ((ret = func(fullpath, &statbuf, FTW_D)) != 0)
		return ret;	
	
//	ptr = fullpath + strlen(fullpath); /* установить указатель в конец fullpath */
//	*ptr++ = '/';
//	*ptr = 0;
	
	if ((dp = opendir(fullpath)) == NULL) /* каталог недоступен */
		return func(fullpath, &statbuf, FTW_DNR);

	chdir (fullpath);	
	while (((dirp = readdir(dp)) != NULL) && (ret == 0))
	{
		//err_msg("%d", level);
		if (!(strcmp(dirp->d_name, ".") == 0 || strcmp(dirp->d_name, "..") == 0))
		{
		
			strcpy(fullpath, dirp->d_name);
			/* скопировать имя каталога */
		
			for (i = 0; i < level; ++i)
				printf("\t");
			printf("%s\n", fullpath);
			/* вывести полный путь к текущему файлу */

			ret = dopath(func, level + 1); /* рекурсия */
			/* выход по ошибке */

		}
	}
	
	chdir("..");
	//err_msg("Cycle exit, level: %d, fullpath: %s\n", level, fullpath);
	
	if (closedir(dp) < 0)
		err_ret("невозможно закрыть каталог %s", fullpath);
	
	return ret;
}

static int myfunc (const char *pathname, const struct stat *statptr, int type)
{
	switch (type) 
	{
		case FTW_F:
			switch (statptr->st_mode & S_IFMT) 
			{
				case S_IFREG: nreg++; break;
				case S_IFBLK: nblk++; break;
				case S_IFCHR: nchr++; break;
				case S_IFIFO: nfifo++; break;
				case S_IFLNK: nslink++; break;
				case S_IFSOCK: nsock++; break;
				case S_IFDIR:
				err_dump("признак S_IFDIR для %s", pathname);
			/* каталоги должны иметь тип = FTW_D */
			}
		break;
		
		case FTW_D: ndir++; break;
		
		case FTW_DNR:
			err_ret("закрыт доступ к каталогу %s", pathname);
		break;
		
		case FTW_NS:
			err_ret("ошибка вызова функции stat для %s", pathname);
		break;
		
		default:
			err_dump("неизвестный тип %d для файла %s", type, pathname);
	}
	return 0;
}
