#include <stdio.h>

int main()
{
  FILE *fs[2];
  fs[0] = fopen ("alphabet2.txt", "w");
  fs[1] = fopen ("alphabet2.txt", "w");
  
  int curr = 0;  
	
	
  for (char c = 'a'; c <= 'z'; ++c)
  {
	  fprintf (fs[curr], "%c", c);
	  curr = ((curr == 0) ? 1 : 0);
  }

	
  fclose(fs[0]);
  fclose(fs[1]);
	
  return 0;
}