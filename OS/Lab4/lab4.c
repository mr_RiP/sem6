#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <linux/random.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");

MODULE_DESCRIPTION("Fortune Cookie Kernel Linux");

MODULE_AUTHOR("Dmitry Frolov");

#define MAXLEN PAGE_SIZE
static struct proc_dir_entry* proc_entry;

static char* curr_str;
static int pos = 0;

struct proc_dir_entry *mydir;
struct proc_dir_entry *mysubdir; 
struct proc_dir_entry *mysym;

static inline char get_random_char (void)
{
	static const char interval = 'z' - 'a' + 1;
	
	unsigned char byte;
	get_random_bytes (&byte, 1);
	
	return 'a' + (byte % interval);
}

static inline int isnum (char c)
{
	return ((c >= '0') && (c <= '9')) ? 1 : 0;
}

static inline int atoi (const char* str)
{
	int res = 0;
	
	while (isnum (*str))
	{
		res *= 10;
		res += *str++ - '0';
	}
	
	printk (KERN_DEBUG "lab4 debug: %d", res);
	
	return res;
}

ssize_t fortune_write (struct file *filp, const char __user *buff, unsigned long len, void *data)
{

	if (copy_from_user(curr_str, buff, len))
		return -EFAULT;

	int count = atoi (curr_str);
	int i;
	
	if (pos + count + 1 > MAXLEN)
	{
		printk (KERN_ERR "lab4: buffer is not large enough");
		return -ENOSPC;
	}

	for (i = 0; i < count; ++i, ++pos)
		curr_str[pos] = get_random_char();
		
	return len;
}

int fortune_read (char *page, char **start, off_t off, int count, int *eof, void *data)
{
  	int len;
  
  	if (off > 0) 
  	{
		*eof = 1;
		return 0;
  	}

	len = sprintf(page, "%s\n", curr_str);
	memset (curr_str, '\0', pos);
	pos = 0;
	
	return len;
}

int init_fortune_module (void)
{
	mydir = proc_mkdir("mymodule", NULL);   
  	mysubdir = proc_mkdir("mysubdir", mydir); 
  	mysym = proc_symlink("mysymlink", NULL, "/proc/mymodule"); 

	curr_str = (char *) vmalloc (MAXLEN);
	
	if (!curr_str)
		return -ENOMEM;
	
	memset(curr_str, '\0', MAXLEN);
	proc_entry = create_proc_entry("lab4", 0777, NULL);
		
	if (proc_entry == NULL)
	{
		vfree(curr_str);
		printk(KERN_ERR "lab4: Couldn't create proc entry\n");
		return -ENOMEM;
	}
	
	proc_entry->read_proc = fortune_read; // callback
	proc_entry->write_proc = fortune_write; // callback

	printk(KERN_INFO "lab4: Module loaded.\n");
	
	return 0;
}

void cleanup_fortune_module (void)
{
	remove_proc_entry("lab4", NULL);

	remove_proc_entry("mysubdir", mydir); 
  	remove_proc_entry("mymodule", NULL); 
  	remove_proc_entry("mysymlink", NULL); 

	vfree(curr_str);
	printk(KERN_INFO "lab4: Module unloaded.\n");
}

module_init (init_fortune_module);
module_exit (cleanup_fortune_module);
