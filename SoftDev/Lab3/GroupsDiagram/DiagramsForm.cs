﻿using PluginSDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupsDiagram
{
    public partial class DiagramsForm : Form
    {
        public DiagramsForm()
        {
            InitializeComponent();
        }

        public IReadOnlyList<IStudent> ModelData { get; set; }

        public void Reset()
        {
            BeginInvoke(new MethodInvoker(() =>
            {
                ResetCountChart();
                ResetRanksChart();
            }));
        }

        private void ResetRanksChart()
        {
            var table = new DataTable();
            table.Columns.Add("Группа", typeof(string));
            table.Columns.Add("Баллы", typeof(float));
            ModelData
                .Where(o => !o.IsEmpty())
                .GroupBy(o => o.Group, o => o.Rating, (key, gr) => new
                {
                    GroupName = key,
                    Rating = gr
                        .Select(o =>
                        {
                            float val;
                            bool success = float.TryParse(o, out val);
                            return new { Value = val, Success = success };
                        })
                        .Where(o => o.Success)
                        .Select(o => o.Value)
                })
                .Select(o => new
                {
                    GroupName = o.GroupName,
                    Rating = o.Rating.Sum() / (float)o.Rating.Count()
                })
                .ToList()
                .ForEach(o => table.Rows.Add(o.GroupName, o.Rating));
            midRankChart.Series[0].XValueMember = "Группа";
            midRankChart.Series[0].YValueMembers = "Баллы";
            midRankChart.DataSource = table;
            midRankChart.DataBind();
        }

        private void ResetCountChart()
        {
            var table = new DataTable();
            table.Columns.Add("Группа", typeof(string));
            table.Columns.Add("Количество", typeof(int));
            ModelData
                .Where(o => !o.IsEmpty())
                .GroupBy(o => o.Group)
                .Select(o => new { Group = o.Key, Count = o.Count() })
                .ToList()
                .ForEach(o => table.Rows.Add(o.Group, o.Count));

            studentsCountChart.Series[0].XValueMember = "Группа";
            studentsCountChart.Series[0].YValueMembers = "Количество";
            studentsCountChart.DataSource = table;
            studentsCountChart.DataBind();
        }

        private void DiagramsForm_Load(object sender, EventArgs e)
        {
            Reset();
        }
    }
}
