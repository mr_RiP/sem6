﻿namespace GroupsDiagram
{
    partial class DiagramsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.studentsCountChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.midRankChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.studentsCountChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.midRankChart)).BeginInit();
            this.SuspendLayout();
            // 
            // studentsCountChart
            // 
            chartArea1.Name = "ChartArea1";
            this.studentsCountChart.ChartAreas.Add(chartArea1);
            this.studentsCountChart.Location = new System.Drawing.Point(12, 12);
            this.studentsCountChart.Name = "studentsCountChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Name = "Series1";
            this.studentsCountChart.Series.Add(series1);
            this.studentsCountChart.Size = new System.Drawing.Size(300, 300);
            this.studentsCountChart.TabIndex = 0;
            this.studentsCountChart.Text = "chart1";
            title1.Name = "Title1";
            title1.Text = "Количество студентов в группах";
            this.studentsCountChart.Titles.Add(title1);
            // 
            // midRankChart
            // 
            chartArea2.Name = "ChartArea1";
            this.midRankChart.ChartAreas.Add(chartArea2);
            this.midRankChart.Location = new System.Drawing.Point(318, 12);
            this.midRankChart.Name = "midRankChart";
            series2.ChartArea = "ChartArea1";
            series2.Name = "Series1";
            this.midRankChart.Series.Add(series2);
            this.midRankChart.Size = new System.Drawing.Size(300, 300);
            this.midRankChart.TabIndex = 1;
            this.midRankChart.Text = "chart2";
            title2.Name = "Title1";
            title2.Text = "Средний балл в группах";
            this.midRankChart.Titles.Add(title2);
            // 
            // DiagramsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 323);
            this.Controls.Add(this.midRankChart);
            this.Controls.Add(this.studentsCountChart);
            this.Name = "DiagramsForm";
            this.Text = "Диаграммы групп";
            this.Load += new System.EventHandler(this.DiagramsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.studentsCountChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.midRankChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart studentsCountChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart midRankChart;
    }
}