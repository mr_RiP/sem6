﻿using GroupsDiagram;
using PluginSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupDiagrams
{
    public class GroupDiagramsPlugin : IPlugin
    {
        public string Name
        {
            get
            {
                return "GroupDiagrams";
            }
        }

        private DiagramsForm form;
        private IReadOnlyList<IStudent> model;

        public void Load(IPluginManager manager)
        {
            manager.Menus.AddMainMenuEtcButton("Диаграммы групп", new EventHandler(this.ShowButtonClick));
            model = manager.Model;
            form = new DiagramsForm { ModelData = model };
        }

        private void ShowButtonClick(object sender, EventArgs e)
        {
            if (form.IsDisposed)
                form = new DiagramsForm { ModelData = model };

            form.Show();
        }

        public void Reset()
        {
            if (form.Visible)
                form.Reset();
        }
    }
}
