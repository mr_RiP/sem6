﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginSDK
{
    public interface IStudent
    {
        string FirstName { get; }
        string MiddleName { get; }
        string LastName { get; }
        string Rating { get; }
        string Group { get; }
        string AvatarUrl { get; }

        string Text { get; }
        bool IsEmpty();
        bool IsIncomplete();
    }
}
