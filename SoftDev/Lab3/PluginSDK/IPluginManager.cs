﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginSDK
{
    public interface IPluginManager
    {
        IReadOnlyList<IStudent> Model { get; }
        IMenuManager Menus { get; }

        IStudent SelectedStudent { get; }
        string SelectedGroup { get; }

        void Reset();
    }
}
