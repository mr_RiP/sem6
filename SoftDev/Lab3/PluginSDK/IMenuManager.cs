﻿using System;

namespace PluginSDK
{
    public interface IMenuManager
    {
        void AddMainMenuEtcButton(string name, EventHandler eventHandler);
        void AddContextMenuGroupButton(string name, EventHandler eventHandler);
        void AddContextMenuStudentButton(string name, EventHandler eventHandler);
    }
}