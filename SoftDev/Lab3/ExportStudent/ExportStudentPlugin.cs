﻿using PluginSDK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExportStudent
{
    public class ExportStudentPlugin : IPlugin
    {
        public string Name
        {
            get
            {
                return "ExportStudent";
            }
        }

        private SaveFileDialog dialog;
        private IPluginManager manager;

        public void Load(IPluginManager manager)
        {
            SetupSaveFileDialog();
            this.manager = manager;
            manager.Menus.AddContextMenuStudentButton("Экспорт", new EventHandler(ExportStudentButtonClick));
        }

        private void ExportStudentButtonClick(object sender, EventArgs e)
        {
            IStudent student = manager.SelectedStudent;
            if ((StudentCheck(student)) && (dialog.ShowDialog() == DialogResult.OK))
            {
                try
                {
                    ExportStudent(student);
                }
                catch (Exception err)
                {
                    ErrorMessage(err.Message);
                } 
            }
            
        }

        private void ExportStudent(IStudent student)
        {
            using (Stream fileStream = dialog.OpenFile())
            using (StreamWriter writer = new StreamWriter(fileStream))
            {
                if (student.LastName != null)   writer.WriteLine("Фамилия: " + student.LastName);
                if (student.FirstName != null)  writer.WriteLine("Имя: " + student.FirstName);
                if (student.MiddleName != null) writer.WriteLine("Отчество: " + student.MiddleName);
                if (student.Group != null)      writer.WriteLine("Группа: " + student.Group);
                if (student.Rating != null)     writer.WriteLine("Рейтинг: " + student.Rating);
                if (student.AvatarUrl != null)  writer.WriteLine("Аватар: " + student.AvatarUrl);
            }

            SuccessMessage();
        }

        private void SuccessMessage()
        {
            MessageBox.Show("Данные успешно сохранены", "Успех",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private static void ErrorMessage(string errorMessage)
        {
            MessageBox.Show("Ошибка при попытке сохранения.\n[" + errorMessage + "]", "Ошибка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static bool StudentCheck(IStudent student)
        {
            if (student.IsEmpty())
            {
                string message = "Для выбранного студента не указано никаких данных.";
                string caption = "Ошибка";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBoxIcon icon = MessageBoxIcon.Error;
                MessageBox.Show(message, caption, buttons, icon);
                return false;
            }
            else if (student.IsIncomplete())
            {
                string message = "Не все поля данных студента заполнены.\nПродолжить?";
                string caption = "Подтверждение";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                MessageBoxIcon icon = MessageBoxIcon.Warning;
                return MessageBox.Show(message, caption, buttons, icon) == DialogResult.Yes;
            }
            else
                return true;
        }

        private void SetupSaveFileDialog()
        {
            dialog = new SaveFileDialog();

            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Lab3\Export";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            dialog.InitialDirectory = path;
            dialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            dialog.FileName = "ExportStudent.txt";
        }

        public void Reset()
        {
        }
    }
}
