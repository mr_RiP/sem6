﻿using Lab1.Control.Command;
using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control
{
    class CommandManager
    {
        public CommandManager()
        {
            commands = new List<ICommand>();
            pos = 0;
        }

        private List<ICommand> commands;
        private int pos;

        public void Redo()
        {
            if (pos < commands.Count)
                commands[pos++].Execute();
        }

        public void Undo()
        {
            if (pos > 0)
                commands[--pos].Undo();
        }

        public int RedoCommandsCount()
        {
            return commands.Count - pos;
        }

        public int UndoCommandsCount()
        {
            return pos;
        }

        public void ExecuteCommand(ICommand cmd)
        {
            if (pos < commands.Count)
                commands.RemoveRange(pos, commands.Count - pos);

            cmd.Execute();
            commands.Add(cmd);
            pos++;
        }

        public void Reset()
        {
            commands.Clear();
            pos = 0;
        }
    }
}
