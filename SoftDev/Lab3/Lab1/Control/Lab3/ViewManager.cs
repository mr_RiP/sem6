﻿using Lab1.Control.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1.Control.Lab3
{
    class ViewManager
    {
        public ViewManager(TreeView treeView, DataGridView dataGridView)
        {
            Tree = new StudentDataTreeViewAdapter(treeView);
            Properties = new StudentDataGridViewAdapter(dataGridView);
        }

        public StudentDataGridViewAdapter Properties { get; private set; }
        public StudentDataTreeViewAdapter Tree { get; private set; }
    }
}
