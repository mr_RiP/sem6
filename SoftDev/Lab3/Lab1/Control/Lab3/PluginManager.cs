﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Control.Adapter;
using Lab1.Model;
using System.Windows.Forms;
using PluginSDK;
using System.IO;
using System.Reflection;
using Lab1.Control.Lab3;

namespace Lab1.Control.Lab3
{
    class PluginManager : IPluginManager
    {
        public PluginManager(StudentList model, ViewManager views, MenuManager menus)
        {
            Model = model;
            Menus = menus;
            Views = views;

            SetupPlugins();
        }

        public IMenuManager Menus { get; private set; }
        public IReadOnlyList<IStudent> Model { get; private set; }
        public IReadOnlyList<IPlugin> Plugins { get; private set; }
        public ViewManager Views { get; private set; }

        public IStudent SelectedStudent
        {
            get
            {
                return Views.Tree.IsStudentSelected() ? Views.Tree.SelectedStudent : null;
            }
        }

        public string SelectedGroup
        {
            get
            {
                return Views.Tree.IsGroupSelected() ? Views.Tree.SelectedGroup : null;
            }
        }

        public void Reset()
        {
            foreach (var p in Plugins)
                p.Reset();
        }

        public void SetupPlugins()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Lab3\Plugins";

            string[] dllFileNames = GetDllFileNames(path);
            if (dllFileNames == null) 
                return;
            
            List<Assembly> assemblies = GetAsseblies(dllFileNames);
            List<Type> pluginTypes = GetPluginTypes(assemblies);
            List<IPlugin> plugins = GetUniquePluginInstances(pluginTypes);

            LoadPlugins(plugins);
        }

        private void LoadPlugins(List<IPlugin> plugins)
        {
            foreach (var plugin in plugins)
                plugin.Load(this);

            Plugins = plugins;

            PluginsLoadedMessage();
        }

        private List<IPlugin> GetUniquePluginInstances(List<Type> pluginTypes)
        {
            var plugins = new List<IPlugin>(pluginTypes.Count);
            foreach (Type type in pluginTypes)
            {
                IPlugin plugin = (IPlugin)Activator.CreateInstance(type);
                if (!plugins.Select(o => o.Name).Contains(plugin.Name))
                    plugins.Add(plugin);
            }

            return plugins;
        }

        private List<Type> GetPluginTypes(List<Assembly> assemblies)
        {
            Type pluginType = typeof(IPlugin);
            var pluginTypes = new List<Type>();
            foreach (Assembly assembly in assemblies)
            {
                if (assembly != null)
                {
                    Type[] types = assembly.GetTypes();
                    foreach (Type type in types)
                        if (type.IsInterface || type.IsAbstract)
                            continue;
                        else
                            if (type.GetInterface(pluginType.FullName) != null)
                                pluginTypes.Add(type);
                }
            }

            return pluginTypes;
        }

        private List<Assembly> GetAsseblies(string[] dllFileNames)
        {
            var assemblies = new List<Assembly>();
            foreach (string dllFile in dllFileNames)
            {
                AssemblyName an = AssemblyName.GetAssemblyName(dllFile);
                Assembly assembly = Assembly.Load(an);
                assemblies.Add(assembly);
            }

            return assemblies;
        }

        private string[] GetDllFileNames(string path)
        {
            string[] dllFileNames = null;
            if (Directory.Exists(path))
                dllFileNames = Directory.GetFiles(path, "*.dll");
            else
                Directory.CreateDirectory(path);
            return dllFileNames;
        }

        private void PluginsLoadedMessage()
        {
            if (!Plugins.Any())
                return;

            string message = "Загружены плагины:\n" + string.Join("\n", Plugins.Select(o => o.Name).ToArray());
            MessageBox.Show(message, "Загрузка плагинов");
        }
    }
}
