﻿using PluginSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1.Control.Lab3
{
    class MenuManager : IMenuManager
    {
        public MenuManager(ContextMenuStrip groupСontextMenuStrip, ContextMenuStrip studentContextMenuStrip,
            ToolStripMenuItem addToolStripMenuItem, ToolStripMenuItem etcToolStripMenuItem)
        {
            mainMenuAdd = addToolStripMenuItem;
            mainMenuEtc = etcToolStripMenuItem;
            contextMenuGroup = groupСontextMenuStrip;
            contextMenuStudent = studentContextMenuStrip;

            InitializeMenus();
        }

        private void InitializeMenus()
        {
            if (UserRights.IsAdministrator())
            {
                mainMenuAdd.Visible = true;
                mainMenuEtc.Visible = false;
                foreach (ToolStripItem item in contextMenuGroup.Items)
                    item.Visible = true;
                foreach (ToolStripItem item in contextMenuStudent.Items)
                    item.Visible = true;
            }
            else
            {
                mainMenuAdd.Visible = false;
                mainMenuEtc.Visible = false;
                foreach (ToolStripItem item in contextMenuGroup.Items)
                    item.Visible = false;
                foreach (ToolStripItem item in contextMenuStudent.Items)
                    item.Visible = false;
            }
        }

        public void AddMainMenuEtcButton(string name, EventHandler eventHandler)
        {
            var item = new ToolStripMenuItem(name);
            item.Click += eventHandler;
            mainMenuEtc.DropDownItems.Add(item);
            mainMenuEtc.Visible = true;

        }

        public void AddContextMenuGroupButton(string name, EventHandler eventHandler)
        {
            var item = new ToolStripMenuItem(name);
            item.Click += eventHandler;
            contextMenuGroup.Items.Add(item);
        }

        public void AddContextMenuStudentButton(string name, EventHandler eventHandler)
        {
            var item = new ToolStripMenuItem(name);
            item.Click += eventHandler;
            contextMenuStudent.Items.Add(item);
        }

        private ToolStripMenuItem mainMenuAdd;
        private ToolStripMenuItem mainMenuEtc;
        private ContextMenuStrip contextMenuGroup;
        private ContextMenuStrip contextMenuStudent;

    }
}
