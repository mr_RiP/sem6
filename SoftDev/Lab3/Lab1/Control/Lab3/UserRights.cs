﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control.Lab3
{
    class UserRights
    {
        public UserRights()
        {
            admin = IsAdministrator();
        }

        private bool admin;

        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        public bool CanChangeData { get { return admin; } }
        public bool CanAddGroups { get { return admin; } }
        public bool CanDeleteGroups { get { return admin; } }
        public bool CanAddStudents { get { return admin; } }
        public bool CanDeleteStudents { get { return admin; } }
        public bool CanAddData { get { return CanAddGroups && CanAddStudents; } }
        public bool CanDeleteData { get { return CanDeleteGroups && CanDeleteStudents; } }
    }
}
