﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab1.Model;
using Lab1.Control.Adapter;
using Lab1.Control.Command;
using Lab1.Control.Lab3;

namespace Lab1.Control
{
    public partial class StudentDataController : UserControl
    {
        public StudentDataController()
        {
            InitializeComponent();
            model = new StudentList();
            commander = new CommandManager();
            views = new ViewManager(treeView, dataGridView);
            menus = new MenuManager(groupСontextMenuStrip, studentContextMenuStrip, addToolStripMenuItem, etcToolStripMenuItem);
            plugins = new PluginManager(model, views, menus);
        }

        private StudentList model;
        private CommandManager commander;
        private ViewManager views;
        private PluginManager plugins;
        private MenuManager menus;

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    model.LoadFromXML(openFileDialog.FileName);
                    SuccessMessage("Данные успешно загружены");
                }
                catch (Exception err)
                {
                    model.Clear();
                    ErrorMessage("Ошибка при загрузке данных", err.Message);
                }
                finally
                {
                    commander.Reset();
                    views.Tree.Reset(model);
                    views.Properties.Reset();
                    plugins.Reset();
                }
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (views.Tree.IsGroupSelected())
                views.Properties.Reset(model, views.Tree.SelectedGroup);
            else
                views.Properties.Reset(views.Tree.SelectedStudent);
            plugins.Reset();

        }

        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (model.IsEmpty() || !views.Tree.IsNodeSelected())
                views.Properties.Clear();
            else
            {
                if (views.Tree.IsGroupSelected())
                    UpdateGroupData();
                else
                    UpdateStudentData(e.RowIndex);
                plugins.Reset();
            }
        }

        private void UpdateGroupData()
        {
            if (views.Properties.IsGroupChanged(views.Tree.SelectedGroup))
            {
                var newGroupName = views.Properties.GroupName;
                commander.ExecuteCommand(new GroupRenameCommand(model, views.Tree.SelectedGroup, newGroupName));
                views.Tree.Reset(model);
                views.Tree.SelectGroup(newGroupName);
            }
        }

        private void UpdateStudentData(int rowIndex)
        {
            if (views.Properties.IsStudentDataChanged(views.Tree.SelectedStudent, rowIndex))
            {
                if (!views.Properties.IsRatingDataValid())
                {
                    views.Properties.SetRatingData(views.Tree.SelectedStudent);
                    views.Properties.IncorrectRatingDataMessage();
                }
                else
                {
                    var newStudent = views.Properties.CreateStudent();
                    commander.ExecuteCommand(new StudentEditCommand(model, views.Tree.SelectedStudent, newStudent));
                    views.Tree.Reset(model);
                    views.Tree.SelectStudent(newStudent);
                }
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commander.Undo();
            views.Tree.Reset(model);
            views.Properties.Reset();
            plugins.Reset();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commander.Redo();
            views.Tree.Reset(model);
            views.Properties.Reset();
            plugins.Reset();
        }

        private void addStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewStudent(views.Tree.SelectedGroup);
        }

        private void deleteGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commander.ExecuteCommand(new DeleteGroupCommand(model, views.Tree.SelectedGroup));
            views.Tree.Reset(model);
            views.Properties.Reset();
            plugins.Reset();
        }

        private void deleteStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var group = views.Tree.SelectedGroup;
            commander.ExecuteCommand(new DeleteStudentCommand(model, views.Tree.SelectedStudent));
            views.Tree.Reset(model);
            views.Properties.Reset();
            plugins.Reset();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (model.IsEmpty())
                ErrorMessage("Нет данных для сохранения");
            else
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        model.SaveToXML(saveFileDialog.FileName);
                        SuccessMessage("Данные успешно сохранены");
                    }
                    catch (Exception err)
                    {
                        ErrorMessage("Во время сохранения произошла ошибка", err.Message);
                    }
                }
            }

        }

        private void ErrorMessage(string message, string errorMessage)
        {
            string caption = "Ошибка";
            message = message + "\n[" + errorMessage + "]";
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SuccessMessage(string message)
        {
            string caption = "Успех";
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ErrorMessage(string message)
        {
            string caption = "Ошибка";
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        
        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                views.Tree.SelectNode(e.Node);
                if (views.Tree.IsGroupSelected())
                    groupСontextMenuStrip.Show(treeView, e.Location);
                else
                    studentContextMenuStrip.Show(treeView, e.Location);
            }
        }

        private void addGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewGroup();
        }

        private void addGroupMainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewGroup();
        }

        private void AddNewGroup()
        {
            commander.ExecuteCommand(new NewGroupCommand(model, "Новая группа"));
            views.Tree.Reset(model);
            views.Tree.SelectGroup(model.Groups.Last());
            plugins.Reset();
        }

        private void AddNewStudent(string groupName)
        {
            var student = new Student(groupName);
            commander.ExecuteCommand(new AddStudentCommand(model, student));
            views.Tree.Reset(model);
            views.Tree.SelectStudent(student);
            plugins.Reset();
        }

        private void addStudentMainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (model.IsEmpty())
                ErrorMessage("Необходмо создать группу");
            else
            {
                if (views.Tree.SelectedGroup == null)
                    ErrorMessage("Необходимо предварительно выбрать группу или студента");
                else
                    AddNewStudent(views.Tree.SelectedGroup);
            }
        }
    }
}
