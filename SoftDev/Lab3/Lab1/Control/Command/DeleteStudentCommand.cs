﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control.Command
{
    class DeleteStudentCommand : ICommand
    {
        private DeleteStudentCommand()
        {
        }

        public DeleteStudentCommand(StudentList reciever, Student student)
        {
            this.reciever = reciever;
            this.student = student;

            this.index = -1;
        }

        StudentList reciever;
        Student student;
        int index;

        public void Execute()
        {
            if (index < 0)
                index = reciever.IndexOf(student);

            reciever.Remove(student);
        }

        public void Undo()
        {
            reciever.Insert(index, student);
        }
    }
}
