﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control.Command
{
    class StudentEditCommand : ICommand
    {
        private StudentEditCommand()
        {
        }

        public StudentEditCommand(StudentList reciever, Student before, Student after)
        {
            this.reciever = reciever;
            this.before = before;
            this.after = after;
        }

        private StudentList reciever;
        private Student before;
        private Student after;

        public void Execute()
        {
            reciever[reciever.IndexOf(before)] = after;
        }

        public void Undo()
        {
            reciever[reciever.IndexOf(after)] = before;
        }
    }
}
