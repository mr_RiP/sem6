﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Xml.Linq;
using PluginSDK; 

namespace Lab1.Model
{
    public class Student : IStudent
    {
        public Student()
        {
        }

        public Student(XElement xStudent, string groupName)
        {
            if (groupName == null)
                throw new NullReferenceException();

            FirstName = (string)xStudent.Element("name");
            MiddleName = (string)xStudent.Element("middleName");
            LastName = (string)xStudent.Element("surname");
            Rating = (string)xStudent.Element("rating");
            Group = groupName;
            AvatarUrl = (string)xStudent.Element("avatar");
        }

        public Student(string groupName)
        {
            if (groupName == null)
                throw new NullReferenceException();

            Group = groupName;
        }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Rating { get; set; }

        public string Group { get; set; }

        public string AvatarUrl { get; set; }

        public bool IsEmpty()
        {
            return FirstName == null && MiddleName == null && LastName == null &&
                Rating == null && AvatarUrl == null;
        }

        public bool IsIncomplete()
        {
            return FirstName == null || MiddleName == null || LastName == null ||
                Rating == null || AvatarUrl == null;
        }

        public string Text
        {
            get
            {
                if (IsEmpty())
                    return "<Данные не заданы>";
                else
                    return string.Join(" ", LastName, FirstName, MiddleName);
            }
        }

        public XElement ToXElement()
        {
            var xStudent = new XElement("student");

            if (LastName != null)
                xStudent.Add(new XElement("surname", LastName));
            if (FirstName != null)
                xStudent.Add(new XElement("name", FirstName));
            if (MiddleName != null)
                xStudent.Add(new XElement("middleName", MiddleName));
            if (Rating != null)
                xStudent.Add(new XElement("rating", Rating));
            if (AvatarUrl != null)
                xStudent.Add(new XElement("avatar", AvatarUrl));

            return xStudent;           
        }
    }
}