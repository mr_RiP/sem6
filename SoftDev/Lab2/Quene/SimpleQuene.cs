﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quene
{
    public class SimpleQuene : IQuene
    {
        public SimpleQuene(int maxLength = int.MaxValue)
        {
            MaxLength = maxLength;
            _listFirst = null;
            _listLast = null;
        }

        private SimpleListNode _listFirst;
        private SimpleListNode _listLast;
        private int _maxLength;
        private int _length;

        public List<int> Values
        {
            get
            {
                var list = new List<int>();
                var node = _listFirst;
                while (node != null)
                {
                    list.Add(node.Value);
                    node = node.Next;
                }

                return list;
            }
        }

        public int MaxLength
        {
            get
            {
                return _maxLength;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException(nameof(MaxLength));
                else
                    _maxLength = value;
            }
        }

        public int Length
        {
            get
            {
                return _length;
            }
        }

        public int MinValue
        {
            get
            {
                if (_length == 0)
                    throw new InvalidOperationException();

                return _listLast.Min;
            }
        }

        public int MaxValue
        {
            get
            {
                if (_length == 0)
                    throw new InvalidOperationException();

                return _listLast.Max;
            }
        }

        public int Dequene()
        {
            if (_length == 0)
                throw new InvalidOperationException();

            int value = _listFirst.Value;
            _listFirst = _listFirst.Next;
            _length--;

            return value;
        }

        public void Enquene(int value)
        {
            if (_length == _maxLength)
                throw new InvalidOperationException();

            if (_length == 0)
            {
                var node = new SimpleListNode(value);
                _listLast = node;
                _listFirst = node;
            }
            else
            {
                _listLast.Next = new SimpleListNode(value, _listLast.Max, _listLast.Min);
                _listLast = _listLast.Next;
            }

            _length++;
        }
    }
}
