﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quene
{
    public class SimpleListNode
    {
        public SimpleListNode(int value, SimpleListNode next = null)
        {
            Value = value;
            Max = value;
            Min = value;
            Next = next;
        }

        public SimpleListNode(int value, int currentMax, int currentMin, 
            SimpleListNode next = null)
        {
            Value = value;
            Max = value > currentMax ? value : currentMax;
            Min = value < currentMin ? value : currentMin;
            Next = next;
        }

        public int Value { get; private set; }
        public int Max { get; private set; }
        public int Min { get; private set; }

        public SimpleListNode Next { get; set; }

        public SimpleListNode Last
        {
            get
            {
                var node = this;
                while (node.Next != null)
                    node = node.Next;
                return node;
            }
        }
    }
}
