﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quene
{
    public interface IQuene
    {
        List<int> Values { get; }

        int Dequene();
        void Enquene(int value);

        int MaxLength { get; set; }
        int Length { get; }

        int MinValue { get; }
        int MaxValue { get; }
    }
}
