﻿using Quene;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            IQuene quene = new SimpleQuene();
            Random rand = new Random();

            int randMin = AskUser("Введите нижний предел случайных значений.");
            int randMax = AskUser("Введите верхний предел случайных значений.");
            int n = AskUser("Введите количество элементов очереди.");

            Console.WriteLine();
            Console.WriteLine("Очередь:");
            for (int i = 0; i < n; ++i)
            {
                int value = rand.Next(randMin, randMax);
                Console.Write("{0} ", value);
                quene.Enquene(value);
            }
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Максимальное значение: {0}.", quene.MaxValue);
            Console.WriteLine("Минимальное значение: {0}.", quene.MinValue);

            Console.ReadLine();
        }

        static int AskUser(string message)
        {
            Console.WriteLine(message);
            return int.Parse(Console.ReadLine());
        }
    }
}
