﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Quene;
using System.Collections.Generic;

namespace QueneTest
{
    [TestClass]
    public class IQueneTest
    {
        private IQuene ConcreteQuene()
        {
            // return new NotImplementedQuene();
            return new SimpleQuene();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_MaxLength_Zero()
        {
            int value = 0;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = value;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_MaxLength_NegativeOne()
        {
            int value = -1;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = value;
        }

        [TestMethod]
        public void Test_MaxLength_PositiveOne()
        {
            int value = 1;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = value;
            int result = quene.MaxLength;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_MaxLength_EnqueneOverflow()
        {
            int value = 1;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 3;
            while (value <= 4)
                quene.Enquene(value++);
        }

        [TestMethod]
        public void Test_Enquene_Dequene_OneValueAtTheTime_123()
        {
            var values = new List<int> { 1, 2, 3 };
            var results = new List<int>();

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            foreach (int val in values)
            {
                quene.Enquene(val);
                results.Add(quene.Dequene());
            }

            CollectionAssert.AreEqual(values, results);
        }

        [TestMethod]
        public void Test_Enquene_Dequene_BufferedValues_123()
        {
            List<int> values = new List<int> { 1, 2, 3 };
            List<int> results = new List<int>();

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            values.ForEach(o => quene.Enquene(o));
            while (quene.Length != 0)
                results.Add(quene.Dequene());

            CollectionAssert.AreEqual(values, results);
        }

        [TestMethod]
        public void Test_Enquene_Values_123()
        {
            var values = new List<int> { 1, 2, 3 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            values.ForEach(o => quene.Enquene(o));
            var results = quene.Values;

            CollectionAssert.AreEqual(values, results);
        }

        [TestMethod]
        public void Test_Length_ZeroAfterCreation()
        {
            int value = 0;

            IQuene quene = ConcreteQuene();
            int result = quene.Length;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_Length_Enquene_OneTime()
        {
            int value = 1;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            quene.Enquene(1);
            int result = quene.Length;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_Length_Enquene_ThreeTimes()
        {
            int value = 3;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            for (int i = 1; i <= 3; ++i)
                quene.Enquene(i);
            int result = quene.Length;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_Length_Enquene_ThreeTimes_Dequene_OneTime()
        {
            int value = 2;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            for (int i = 1; i <= 3; ++i)
                quene.Enquene(i);
            quene.Dequene();
            int result = quene.Length;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_Length_Enquene_ThreeTimes_Dequene_ThreeTimes()
        {
            int value = 0;

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            for (int i = 1; i <= 3; ++i)
                quene.Enquene(i);
            for (int i = 1; i <= 3; ++i)
                quene.Dequene();
            int result = quene.Length;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_Dequene_EmptyQuene()
        {
            IQuene quene = ConcreteQuene();
            quene.Dequene();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_MinValue_EmptyQuene()
        {
            IQuene quene = ConcreteQuene();
            int result = quene.MinValue;
        }

        [TestMethod]
        public void Test_MinValue_AtBegin()
        {
            int value = 1;
            var list = new List<int> { 1, 2, 3 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MinValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MinValue_AtEnd()
        {
            int value = -1;
            var list = new List<int> { 3, 2, 6, -1 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MinValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MinValue_Amid()
        {
            int value = 10;
            var list = new List<int> { 40, 20, 10, 30, 15 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MinValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MinValue_AllEqual()
        {
            int value = 2;
            var list = new List<int> { 2, 2, 2 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MinValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MinValue_AtBegin_SomeEqual()
        {
            int value = 4;
            var list = new List<int> { 4, 4, 40, 50 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MinValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MinValue_AtEnd_SomeEqual()
        {
            int value = -4;
            var list = new List<int> { 7, 10, 5, -4, -4, -4 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MinValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_MaxValue_EmptyQuene()
        {
            IQuene quene = ConcreteQuene();
            int result = quene.MaxValue;
        }

        [TestMethod]
        public void Test_MaxValue_AtBegin()
        {
            int value = 8;
            var list = new List<int> { 8, -2, 3 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MaxValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MaxValue_AtEnd()
        {
            int value = 10;
            var list = new List<int> { 3, -2, -6, 10 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MaxValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MaxValue_Amid()
        {
            int value = 100;
            var list = new List<int> { 40, 20, 100, 30, 15 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MaxValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MaxValue_AllEqual()
        {
            int value = 2;
            var list = new List<int> { 2, 2, 2 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MaxValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MaxValue_AtBegin_SomeEqual()
        {
            int value = 4;
            var list = new List<int> { 4, 4, -40, -50 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MaxValue;

            Assert.AreEqual(value, result);
        }

        [TestMethod]
        public void Test_MaxValue_AtEnd_SomeEqual()
        {
            int value = 45;
            var list = new List<int> { 7, 10, 5, 45, 45, 45 };

            IQuene quene = ConcreteQuene();
            quene.MaxLength = 10;
            list.ForEach(o => quene.Enquene(o));
            int result = quene.MaxValue;

            Assert.AreEqual(value, result);
        }
    }
}
