﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Lab1.Model
{
    class StudentList : List<Student>
    {
        public StudentList() : base()
        {
        }

        public StudentList(string fileName)
        {
            LoadFromXML(fileName);
        }

        public void SaveToXML(string fileName)
        {
            var xGroups = new XElement("groups");
            if (this.Count != 0)
            {
                var groups = this.Select(o => o.Group).Distinct().ToArray();
              
                foreach (string group in groups)
                {
                    var xGroup = new XElement("group");
                    xGroup.Add(new XAttribute("name", group));

                    var xStudents = new XElement("students");
                    var students = this.Where(o => o.Group == group).ToArray();
                    foreach (Student student in students)
                        xStudents.Add(student.ToXElement());

                    xGroup.Add(xStudents);
                    xGroups.Add(xGroup);
                }
            }

            var xDoc = new XDocument(xGroups);
            xDoc.Save(fileName);
        }

        public void LoadFromXML(string fileName)
        {
            var doc = XDocument.Load(fileName);
            this.Clear();
            foreach (var group in doc.Root.Elements())
            {
                string groupName = (string)group.FirstAttribute;
                if (group.Elements().Elements().Any())
                {
                    foreach (var student in group.Elements().Elements())
                        this.Add(new Student(student, groupName));  
                }
                else
                    this.Add(new Student(groupName));
            }      
        }

        public bool IsEmpty()
        {
            return this.Count == 0;
        }

        public List<string> Groups
        {
            get
            {
                return this.Select(o => o.Group).Distinct().ToList();
            }
        }
    }
}