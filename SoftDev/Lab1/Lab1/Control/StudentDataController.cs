﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab1.Model;
using Lab1.Control.Adapter;
using Lab1.Control.Command;

namespace Lab1.Control
{
    public partial class StudentDataController : UserControl
    {
        public StudentDataController()
        {
            InitializeComponent();
            model = new StudentList();
            commander = new CommandManager();
            propertiesDataView = new StudentDataGridViewAdapter(dataGridView);
            treeDataView = new StudentDataTreeViewAdapter(treeView);
        }

        private StudentList model;
        private CommandManager commander;
        private StudentDataGridViewAdapter propertiesDataView;
        private StudentDataTreeViewAdapter treeDataView;

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    model.LoadFromXML(openFileDialog.FileName);
                    SuccessMessage("Данные успешно загружены");
                }
                catch (Exception err)
                {
                    model.Clear();
                    ErrorMessage("Ошибка при загрузке данных", err.Message);
                }
                finally
                {
                    commander.Reset();
                    treeDataView.Reset(model);
                    propertiesDataView.Reset();
                }
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeDataView.IsGroupSelected())
                propertiesDataView.Reset(model, treeDataView.SelectedGroup);
            else
                propertiesDataView.Reset(treeDataView.SelectedStudent);

        }

        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (model.IsEmpty() || !treeDataView.IsNodeSelected())
                propertiesDataView.Clear();
            else
            {
                if (treeDataView.IsGroupSelected())
                    UpdateGroupData();
                else
                    UpdateStudentData(e.RowIndex);
            }
        }

        private void UpdateGroupData()
        {
            if (propertiesDataView.IsGroupChanged(treeDataView.SelectedGroup))
            {
                var newGroupName = propertiesDataView.GroupName;
                commander.ExecuteCommand(new GroupRenameCommand(model, treeDataView.SelectedGroup, newGroupName));
                treeDataView.Reset(model);
                treeDataView.SelectGroup(newGroupName);
            }
        }

        private void UpdateStudentData(int rowIndex)
        {
            if (propertiesDataView.IsStudentDataChanged(treeDataView.SelectedStudent, rowIndex))
            {
                if (!propertiesDataView.IsRatingDataValid())
                {
                    propertiesDataView.SetRatingData(treeDataView.SelectedStudent);
                    propertiesDataView.IncorrectRatingDataMessage();
                }
                else
                {
                    var newStudent = propertiesDataView.CreateStudent();
                    commander.ExecuteCommand(new StudentEditCommand(model, treeDataView.SelectedStudent, newStudent));
                    treeDataView.Reset(model);
                    treeDataView.SelectStudent(newStudent);
                }
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commander.Undo();
            treeDataView.Reset(model);
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commander.Redo();
            treeDataView.Reset(model);
        }

        private void addStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewStudent(treeDataView.SelectedGroup);
        }

        private void deleteGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commander.ExecuteCommand(new DeleteGroupCommand(model, treeDataView.SelectedGroup));
            treeDataView.Reset(model);
            propertiesDataView.Reset();
        }

        private void deleteStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var group = treeDataView.SelectedGroup;
            commander.ExecuteCommand(new DeleteStudentCommand(model, treeDataView.SelectedStudent));
            treeDataView.Reset(model);
            treeDataView.SelectGroup(group);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (model.IsEmpty())
                ErrorMessage("Нет данных для сохранения");
            else
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        model.SaveToXML(saveFileDialog.FileName);
                        SuccessMessage("Данные успешно сохранены");
                    }
                    catch (Exception err)
                    {
                        ErrorMessage("Во время сохранения произошла ошибка", err.Message);
                    }
                }
            }

        }

        private void ErrorMessage(string message, string errorMessage)
        {
            string caption = "Ошибка";
            message = message + "\n[" + errorMessage + "]";
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SuccessMessage(string message)
        {
            string caption = "Успех";
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ErrorMessage(string message)
        {
            string caption = "Ошибка";
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        
        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                treeDataView.SelectNode(e.Node);
                if (treeDataView.IsGroupSelected())
                    groupСontextMenuStrip.Show(treeView, e.Location);
                else
                    studentContextMenuStrip.Show(treeView, e.Location);
            }
        }

        private void addGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewGroup();
        }

        private void addGroupMainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewGroup();
        }

        private void AddNewGroup()
        {
            commander.ExecuteCommand(new NewGroupCommand(model, "Новая группа"));
            treeDataView.Reset(model);
            treeDataView.SelectGroup(model.Groups.Last());
        }

        private void AddNewStudent(string groupName)
        {
            var student = new Student(groupName);
            commander.ExecuteCommand(new AddStudentCommand(model, student));
            treeDataView.Reset(model);
            treeDataView.SelectStudent(student);
        }

        private void addStudentMainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (model.IsEmpty())
                ErrorMessage("Необходмо создать группу");
            else
            {
                if (treeDataView.SelectedGroup == null)
                    ErrorMessage("Необходимо предварительно выбрать группу или студента");
                else
                    AddNewStudent(treeDataView.SelectedGroup);
            }
        }
    }
}
