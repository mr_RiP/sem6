﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1.Control.Adapter
{
    class StudentDataTreeViewAdapter
    {
        private StudentDataTreeViewAdapter()
        {
        }

        public StudentDataTreeViewAdapter(TreeView view)
        {
            this.view = view;
        }

        private TreeView view;

        public void Reset(StudentList model)
        {
            view.Nodes.Clear();

            if (model == null || model.IsEmpty())
                return;

            // var groups = model.GroupBy(student => student.Group);

            var groups = model.Select(s => s.Group).Distinct().ToArray();
            foreach (var group in groups)
            {
                var students = model.Where(s => s.Group == group).ToArray();
                var studentNodes = new List<TreeNode>();
                foreach (Student student in students)
                {
                    var node = new TreeNode(student.Text);
                    node.Tag = student;
                    studentNodes.Add(node);
                }
                view.Nodes.Add(new TreeNode(group, studentNodes.ToArray()));
            }

            ExpandAllGroups();  
        }

        public bool IsNodeSelected()
        {
            return view.SelectedNode != null;
        }

        public bool IsGroupSelected()
        {
            return view.SelectedNode.Tag == null;
        }

        public bool IsStudentSelected()
        {
            return !IsGroupSelected();
        }

        public void SelectGroup(string groupName)
        {
            view.SelectedNode = view.Nodes.Cast<TreeNode>().First(o => o.Text == groupName);
        }

        public Student SelectedStudent
        {
            get
            {
                return (view.SelectedNode == null)? null: (Student)view.SelectedNode.Tag;
            }
        }

        public string SelectedGroup
        {
            get
            {
                if (view.SelectedNode == null)
                    return null;
                else
                    return (view.SelectedNode.Tag == null)? view.SelectedNode.Text: SelectedStudent.Group;
            }
        }

        public void SelectStudent(Student student)
        {
            view.SelectedNode = view.Nodes.Cast<TreeNode>().SelectMany(o => o.Nodes.Cast<TreeNode>()).First(o => o.Tag == student);
        }

        public void ExpandAllGroups()
        {
            foreach (TreeNode node in view.Nodes)
            {
                node.Expand();
            }
        }

        public void SelectNode(TreeNode node)
        {
            view.SelectedNode = node;
        }
    }
}
