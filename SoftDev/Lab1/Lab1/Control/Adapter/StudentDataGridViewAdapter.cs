﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1.Control.Adapter
{
    class StudentDataGridViewAdapter
    {
        private StudentDataGridViewAdapter()
        {
        }

        public StudentDataGridViewAdapter(DataGridView view)
        {
            this.view = view;
            view.AllowUserToAddRows = false;
        }

        private DataGridView view;

        public void Reset()
        {
            view.ClearSelection();
            view.Rows.Clear();
        }
        
        public void Reset(StudentList model, string groupName)
        {
            view.BeginInvoke(new MethodInvoker(() =>
            {
                PrintGroupData(model, groupName);
                SetPropertyNamesStyle();
                SetReadonlyValuesStyle();
            }));
        }

        public void Reset(Student student)
        {
            view.BeginInvoke(new MethodInvoker(() =>
            { 
                PrintStudentData(student);
                SetPropertyNamesStyle();
            }));
        }

        public bool IsGroupChanged(string originalGroupName)
        {
            return (string)view.Rows[0].Cells[1].Value != originalGroupName;
        }
        
        private void PrintGroupData(StudentList model, string groupName)
        {
            var ratings = model.Where(o => o.Group == groupName).Select(o => o.Rating).ToList();
            var intRatings = ratings
                .Select(o =>
                {
                    int val;
                    bool success = int.TryParse(o, out val);
                    return new { val, success };
                })
                .Where(p => p.success).Select(p => p.val).ToList();
            bool anyValues = intRatings.Any();

            view.ClearSelection();
            view.Rows.Clear();
            view.Rows.Add("Индекс", groupName);
            view.Rows.Add("Количество студентов", intRatings.Count.ToString() + " (" + ratings.Count.ToString() + ")");
            view.Rows.Add("Минимальный рейтинг", anyValues ? intRatings.Min().ToString() : null);
            view.Rows.Add("Максимальный рейтинг", anyValues ? intRatings.Max().ToString() : null);
            view.Rows.Add("Средний рейтинг", anyValues ? ((float)intRatings.Sum() / intRatings.Count()).ToString() : null);
        }

        private void PrintStudentData(Student student)
        {
            view.ClearSelection();
            view.Rows.Clear();
            view.Rows.Add("Фамилия", student.LastName);
            view.Rows.Add("Имя", student.FirstName);
            view.Rows.Add("Отчество", student.MiddleName);
            view.Rows.Add("Группа", student.Group);
            view.Rows.Add("Рейтинг", student.Rating);
            view.Rows.Add("Аватар", student.AvatarUrl);
        }

        public bool IsStudentDataChanged(Student student, int rowIndex)
        {
            object value = view.Rows[rowIndex].Cells[1].Value;
            switch (rowIndex)
            {
                case 0: return (string)value != student.LastName;
                case 1: return (string)value != student.FirstName;
                case 2: return (string)value != student.MiddleName;
                case 3: return (string)value != student.Group;
                case 4: return (string)value != student.Rating;
                case 5: return (string)value != student.AvatarUrl;
                default: return false;
            }
        }

        public Student CreateStudent()
        {
            return new Student
            {
                LastName = (string)view.Rows[0].Cells[1].Value,
                FirstName = (string)view.Rows[1].Cells[1].Value,
                MiddleName = (string)view.Rows[2].Cells[1].Value,
                Group = (string)view.Rows[3].Cells[1].Value,
                Rating = (string)view.Rows[4].Cells[1].Value,
                AvatarUrl = (string)view.Rows[5].Cells[1].Value
            };
        }

        public void IncorrectRatingDataMessage()
        {
            MessageBox.Show("Значение рейтинга студента должно быть целым числом от 0 до 100",
                "Некорректный ввод", MessageBoxButtons.OK, MessageBoxIcon.Error);
  
        }

        public string GroupName
        {
            get { return (string)view.Rows[0].Cells[1].Value; }
        }

        public bool IsRatingDataValid()
        {
            var data = (string)view.Rows[4].Cells[1].Value;
            int numericData;
            return (data == null) || (int.TryParse(data, out numericData) && numericData >= 0 && numericData <= 100);
        }

        public void SetRatingData(Student student)
        {
            view.Rows[4].Cells[1].Value = student.Rating;
        }

        public bool IsRatingDataRow(int row) 
        {
            return row == 4;
        }
        
        public void Clear()
        {
            view.Rows.Clear();
        }

        private void SetPropertyNamesStyle()
        {
            var propNameStyle = new DataGridViewCellStyle();
            propNameStyle.Font = new System.Drawing.Font(view.Font, System.Drawing.FontStyle.Bold);
            foreach (DataGridViewRow row in view.Rows)
                row.Cells[0].Style = propNameStyle;
        }

        private void SetReadonlyValuesStyle()
        {
            view.Rows[0].Cells[1].ReadOnly = false;
            var readonlyCellsStyle = new DataGridViewCellStyle();
            readonlyCellsStyle.Font = new System.Drawing.Font(view.Font, System.Drawing.FontStyle.Italic);
            for (int i = 1; i < view.Rows.Count; ++i)
            {
                view.Rows[i].Cells[1].ReadOnly = true;
                view.Rows[i].Cells[1].Style = readonlyCellsStyle;
            }
        }
    }
}
