﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control.Command
{
    class GroupRenameCommand : ICommand
    {

        private GroupRenameCommand()
        {
        }

        public GroupRenameCommand(StudentList reciever, string before, string after)
        {
            this.reciever = reciever;
            this.before = before;
            this.after = after;
        }

        private StudentList reciever;
        private string before;
        private string after;
        private List<Student> changed;

        public void Execute()
        {
            if (changed == null)
                changed = reciever.Where(o => o.Group == before).ToList();
            changed.ForEach(o => o.Group = after);
        }

        public void Undo()
        {
            changed.ForEach(o => o.Group = before);
        }
    }
}
