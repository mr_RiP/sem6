﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control.Command
{
    class AddStudentCommand : ICommand
    {
        private AddStudentCommand()
        {
        }

        public AddStudentCommand(StudentList reciever, Student student)
        {
            this.reciever = reciever;
            this.student = student;
        }

        StudentList reciever;
        Student student;

        public void Execute()
        {
            reciever.Add(student);
        }

        public void Undo()
        {
            reciever.Remove(student);
        }
    }
}
