﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control.Command
{
    class NewGroupCommand : ICommand
    {
        private NewGroupCommand()
        {
        }

        public NewGroupCommand(StudentList reciever, string groupName)
        {
            this.reciever = reciever;
            this.groupName = groupName;
        }

        private StudentList reciever;
        private string groupName;

        public void Execute()
        {
            if (!reciever.Where(o => o.Group == groupName).Any())
                reciever.Add(new Student(groupName));
            else
            {
                int i = 1;
                string baseName = groupName;
                while (reciever.Where(o => o.Group == groupName).Any())
                    groupName = baseName + " (" + i++.ToString() + ")";
                reciever.Add(new Student(groupName));
            }
        }



        public void Undo()
        {
            reciever.RemoveAll(o => o.Group == groupName);
        }
    }
}
