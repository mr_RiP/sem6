﻿using Lab1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Control.Command
{
    class DeleteGroupCommand : ICommand
    {
        private DeleteGroupCommand()
        {
        }

        public DeleteGroupCommand(StudentList reciever, string groupName)
        {
            this.reciever = reciever;
            this.groupName = groupName;

            deletedStudents = null;
        }

        private StudentList reciever;
        private string groupName;
        private List<Tuple<int,Student>> deletedStudents;

        public void Execute()
        {
            // Оптимизация для Redo - если дейcтвие выполнялось - у нас еще есть данные для Undo
            if (deletedStudents == null)
                deletedStudents = reciever.Where(o => o.Group == groupName)
                    .Select(o => new Tuple<int, Student>(reciever.IndexOf(o), o)).ToList();

            reciever.RemoveAll(o => o.Group == groupName);
        }

        public void Undo()
        {
            foreach (var tup in deletedStudents)
                reciever.Insert(tup.Item1, tup.Item2);
        }
    }
}
