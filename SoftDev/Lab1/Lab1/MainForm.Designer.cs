﻿namespace Lab1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.studentDataController1 = new Lab1.Control.StudentDataController();
            this.SuspendLayout();
            // 
            // studentDataController1
            // 
            this.studentDataController1.Location = new System.Drawing.Point(0, 1);
            this.studentDataController1.Name = "studentDataController1";
            this.studentDataController1.Size = new System.Drawing.Size(689, 362);
            this.studentDataController1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 365);
            this.Controls.Add(this.studentDataController1);
            this.Name = "MainForm";
            this.Text = "Список студентов";
            this.ResumeLayout(false);

        }

        #endregion

        private Control.StudentDataController studentDataController1;
    }
}

