data = load('dataX.txt');

dataMax = max(data);
dataMin = min(data);

dist = dataMax - dataMin;
expect = mean(data);
variance = var(data);
groupsCount = floor(log2(size(data, 2))) + 2;

fprintf('min = %f\n', dataMin);
fprintf('max = %f\n', dataMax);
fprintf('mu = %f\n', expect);
fprintf('var = %f\n', variance);
fprintf('R = %f\n', dist);

x = dataMin:(dist / 99):dataMax;
normDistr = normcdf(x, expect, variance);
[estFunc, estX] = ecdf(data);

subplot(2, 1, 1);
histfit(data, groupsCount);
subplot(2, 1, 2);
hold on
plot(x, normDistr);
stairs(estX, estFunc);
hold off
grid on