data = load('dataX.txt');
dataSize = size(data, 2);

gamma = 0.9;
alpha = (1 - gamma) / 2;
start = 10;
arraySize = start:dataSize;

expect = mean(data);
variance = var(data);

mu = zeros(1, dataSize - start + 1);
for i = arraySize
    mu(i - start + 1) = mean(data(1:i));
end;

s = zeros(1, dataSize - start + 1);
for i = arraySize
    s(i - start + 1) = var(data(1:i));
end;

expectLowBound = mu - sqrt(s ./ arraySize) .* tinv(1 - alpha, arraySize - 1);
expectHighBound = mu + sqrt(s ./ arraySize) .* tinv(1 - alpha, arraySize - 1);

sLowBound = s .* (arraySize - 1) ./ chi2inv(1 - alpha, arraySize - 1);
sHighBound = s .* (arraySize - 1) ./ chi2inv(alpha, arraySize - 1);

subplot(2, 1, 1);
hold on;
plot([arraySize(1), arraySize(end)], [expect, expect]);
plot(arraySize, mu);
plot(arraySize, expectLowBound);
plot (arraySize, expectHighBound);
hold off;
grid on;
title ('Expectation');

subplot(2, 1, 2);
hold on;
plot([arraySize(1), arraySize(end)], [variance, variance]);
plot(arraySize, s);
plot(arraySize, sLowBound);
plot (arraySize, sHighBound);
hold off
grid on
title('Variance');

fprintf('expectation low bound = %f\n', expectLowBound(end));
fprintf('expectation high bound = %f\n', expectHighBound(end));
fprintf('variance low bound = %f\n', sLowBound(end));
fprintf('variance high bound = %f\n', sHighBound(end));