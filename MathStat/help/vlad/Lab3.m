T = load('dataT.txt');
Y = load('dataY.txt');
dd = 1:length(Y);
for i=dd
   if Y(1,i)<-10
      Y(1,i)=-30; 
   end
end
for i=dd
   if T(1,i)>5
      Y(1,i)=-30; 
   end
end

for i=dd
   if (T(1,i)>-1)&&(T(1,i)<1)
      Y(1,i)=-30; 
   end
end
 

  
T0 = ones(1,length(T));
T2 = T.^2;
T3 = T.^3;
T4 = T.^4;

One (1, 1:length(T)) = 1;
F = vertcat(One, T, T2, T3, T4);

FtF = F * transpose(F);
theta = mldivide(FtF, F) * transpose(Y);
Yt = theta(1) + theta(2) * T + theta(3) * T2 + theta(4) * T3 + theta(5) * T4;
delta = sqrt(sum((Y - Yt).^2));
 
plot(T, Y, '.r');
hold on;
 
T_G = min(T):0.1:max(T);
T_G2 = T_G.^2;
T_G3 = T_G.^3;
T_G4 = T_G.^4;
Y_G = theta(1) + theta(2) * T_G + theta(3) * T_G2 + theta(4) * T_G3 + theta(5) * T_G4;
plot(T_G, Y_G, 'b');
hold off;
axis tight; 
grid on;
title('Least squares evaluation');
xlabel(['y = ', num2str(theta(1)), ' + ', num2str(theta(2)), ...
    ' * x + ', num2str(theta(3)), ' * x^2', ' + ', num2str(theta(4)),...
    ' * x^3 + ', num2str(theta(5)), ' * x^4']);
